module Main where

import Control.Concurrent (threadDelay)
import Control.Monad (forever)
import Data.IORef (newIORef, readIORef, writeIORef)
import System.IO (stdin, stdout, hReady, hPutStr, hGetChar, hPutChar, hFlush, hSetBuffering, hSetEcho, Handle(), BufferMode(..))
import qualified System.Console.ANSI as Console

data Tile = Grass
          | Wall
          | Empty

instance Read Tile where
  readsPrec _ "." = [(Grass, "")]
  readsPrec _ "#" = [(Wall, "")]
  readsPrec _ _   = [(Empty, "")]

instance Show Tile where
  show Grass  = "."
  show Wall   = "#"
  show Empty  = " "

data Map = Map {
                 width  :: Int,
                 height :: Int,
                 contents :: [[Tile]]
               }

data Object = Player { x :: Int, y :: Int }
            | Chest  { x :: Int, y :: Int }

instance Show Object where
  show (Player _ _) = "☃"
  show (Chest _ _) = "?"

type Scene = (Map, Object, [Object])

chunksOf :: Int -> [a] -> [[a]]
chunksOf n [] = []
chunksOf n l = (take n l):(chunksOf n (drop n l))

createMap :: Int -> Int -> String -> Map
createMap w h c =
  Map w h (chunksOf w $ map (read . (:[])) c)

renderMap :: Handle -> Map -> IO ()
renderMap theScreen theMap = do
  let xcoords = [0..((width theMap)-1)]
  let ycoords = [0..((height theMap)-1)]
  let allCoords = concatMap (\ycoord -> zip xcoords $ replicate (width theMap) ycoord) ycoords
  mapM_ (drawItem theMap theScreen) allCoords
  where
    drawItem theMap theScreen (x,y) = do
      let item = (((contents theMap) !! y) !! x)
      Console.hSetCursorPosition theScreen y x
      hPutStr theScreen (show item)

renderObject :: Handle -> Object -> IO ()
renderObject theScreen obj = do
  Console.hSetCursorPosition theScreen (y obj) (x obj)
  hPutStr theScreen (show obj)

renderScene :: Handle -> Scene -> IO ()
renderScene theScreen (map, player, objects) = do
  renderMap theScreen map
  mapM_ (renderObject theScreen) objects
  renderObject theScreen player

getCharacter :: Handle -> IO Char
getCharacter handle = do
  r <- hReady handle
  if r then
    hGetChar handle
  else return ' '

handleInput :: Char -> Scene -> Scene
handleInput 'w' (map, (Player x y), objects) = (map, (Player x (y-1)), objects)
handleInput 'a' (map, (Player x y), objects) = (map, (Player (x-1) y), objects)
handleInput 's' (map, (Player x y), objects) = (map, (Player x (y+1)), objects)
handleInput 'd' (map, (Player x y), objects) = (map, (Player (x+1) y), objects)
handleInput _ x = x

screen = stdout
keyboard = stdin
frameRate = round $ 1.0e6/24.0

main = do
  hSetBuffering keyboard NoBuffering
  hSetBuffering screen LineBuffering
  hSetEcho screen False
  Console.hHideCursor screen
  gameScene <- newIORef $ theScene
  forever $ do
    Console.hClearScreen screen
    c <- getCharacter keyboard
    gscene <- readIORef gameScene
    let newScene = handleInput c gscene
    renderScene screen newScene
    hFlush screen
    writeIORef gameScene newScene
    threadDelay frameRate

theMap =
  "                    " ++
  "   ###########      " ++
  "  #...........#     " ++
  "  #............#    " ++
  "  #.............#   " ++
  "  #.............#   " ++
  "  #.............#   " ++
  "   #............#   " ++
  "    #...........#   " ++
  "     ###########    " ++
  "                    "

theObjects = [Chest 10 7, Chest 13 6]

theScene = (createMap 20 10 theMap, Player 10 5, theObjects)
